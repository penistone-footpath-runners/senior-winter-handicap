# Winter Handicap
# ===============
#
# Stores various constants needed by some files

import times

WORSTHANDICAP = times.Time("52:00")
GENDERS = {"F": "Female", "M": "Male"}
SORTS = {
    "Race Result": (
        "Finish Time",
        ("Position", "Name", "Estimated Time", "Finish Time", "Actual Time", "+/−"),
        lambda x: True,
    ),
    "Fastest Times Female": (
        "Actual Time",
        ("Position", "Name", "Actual Time"),
        lambda x: x["Gender"] == "F",
    ),
    "Fastest Times Male": (
        "Actual Time",
        ("Position", "Name", "Actual Time"),
        lambda x: x["Gender"] == "M",
    ),
    "WMA Adjusted Times": (
        "Adjusted Time",
        ("Position", "Name", "Actual Time", "Adjusted Time", "Points"),
        lambda x: True,
    ),
}

RACECOUNT = 8
