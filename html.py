#!/usr/bin/env python3
import os

from constants import *

FHEAD = '<head><meta http-equiv="Content-Type" content="charset=utf-8"><link rel="icon" href="../icon.png"><title>PFR Winter Handicap</title><link rel="stylesheet" href="../style.css"></head><body><div style="width:800px;text-align:center;margin:auto"><img style="float:left;padding-right:20" src="../logo.png" alt="Penistone Footpath Runners & AC" width="128px"><h1 align="left">Penistone Footpath Runners &amp; AC</h1><h2 align="left">2019–20 Senior Winter Handicap</h2><table><th><a href="http://pfrac.co.uk/club-competitions/winter-handicap/" target="_blank">Race and WMA Information</a></th></table><br><br>'

NUMBERS = "0123456789"


def create_html_table(
    data,
    headingrow=0,
    columnalign=None,
    link=True,
    highlight=False,
    path="html/",
    linkables=[],
    numberlink=False,
    rotateheads=[],
    tableinfo="",
    sticky=True,
    bigbold=False,
):
    """Creates an HTML table of some data"""
    if columnalign is None:
        columnalign = ["text-align:left"]
    while len(columnalign) < len(data[0]):
        columnalign.append("text-align:center")

    # Determine how many padding (wierd) spaces are needed to fit all
    # of the numbers
    digitsneeded = []
    for i in range(0, len(data[0]), 1):
        digitsneeded.append(
            max(
                len(str(round(x[i]))) if type(x[i]) in (float, int) else 0 for x in data
            )
        )
    table = f"<table {tableinfo}>"
    for i, row in enumerate(data):
        table += "<tr>"
        for j, cell in enumerate(row):
            text = str(cell)

            if type(cell) == int:
                text = text.rjust(digitsneeded[j], " ")

            # Make it a link if needed
            if link and (os.path.exists(path + text + ".html") or text in linkables):
                text = f'<a href="{text}.html">{text}</a>'

            # Number links
            if (
                numberlink
                and any((x in text for x in NUMBERS))
                and (numberlink[0] + text).replace(numberlink[1], "") in linkables
            ):
                text = f'<a href="{numberlink[0]} {"".join([x if x in NUMBERS else "" for x in text])}.html">{text}</a>'

            # Create boldness
            if highlight:
                if row in highlight:
                    text = f"<strong>{text}</strong>"

            # Make the largest number bold
            if type(cell) == float and bigbold:
                if (
                    sorted(filter(lambda x: type(x) == float, row), reverse=True)[0]
                    == cell
                ):
                    text = f"<strong>{text}</strong>"

            # Do rotated text
            ##            if j in rotateheads and i == 0:
            ##                text = f'<div style="transform: rotate(-90deg);border-collapse:separate;height:150px;table-layout:fixed">{text}</div>'

            # Create if heading
            if i == headingrow:
                if sticky:
                    table += f'<th class="sticky" style="{columnalign[j]}">{text}</th>'
                else:
                    table += f'<th style="{columnalign[j]}">{text}</th>'

            # Empty cell optimisation
            elif text == "":
                table += "<td></td>"

            # Create standard cell
            else:
                table += f'<td style="{columnalign[j]}">{text}</td>'

        table += "</tr>"

    table += "</table>"
    return table


def create_html_page(
    pagename,
    data,
    sortmethod,
    r,
    validraces,
    headingrow=0,
    columnalign=None,
    link=True,
    highlight=False,
    path="html/",
    linkables=[],
    header="",
    numberlink=False,
    rotateheads=[],
    fellhead=False,
    bigbold=False,
):
    """Create an HTML page for some data"""
    nonraces = ["League Table", "Fastest Female", "Fastest Male"]
    races = nonraces + [f"Race {x}" for x in range(1, RACECOUNT + 1)]
    toptable = (
        '<table style="border-collapse:separate;width:800px;table-layout:fixed"><tr>'
    )
    for race in range(0, RACECOUNT + len(nonraces)):
        real_race = race - len(nonraces) + 1
        if real_race < 1:
            real_race = None

        if real_race is not None and real_race not in validraces:
            toptable += f"<th>Race {real_race}</th>"
            continue

        # Create the link part
        if race < len(
            nonraces
        ):  # Link to the league and others, which has no sort order
            link = f'<a href="{races[race]}.html">'
        elif r in nonraces:  # Link from the
            # league and others, which means picking a default sort order
            link = f'<a href="{races[race]} – Race Result.html">'
        else:
            link = f'<a href="{races[race]} – {sortmethod}.html">'

        # Highlight or not
        if races[race] == r:
            toptable += f"<th class=selected>{link}{races[race]}</a></th>"
        else:
            toptable += f"<th>{link}{races[race]}</a></th>"

    toptable += "</tr></table>"

    # Create the sort order table
    if r not in nonraces:
        toptable += '<br><br><table style="border-collapse:separate;width:800px;table-layout:fixed"><tr>'
        for sort in SORTS:
            if sort == sortmethod:
                toptable += (
                    f'<th class=selected><a href="{r} – {sort}.html">{sort}</a></th>'
                )
            else:
                toptable += f'<th><a href="{r} – {sort}.html">{sort}</a></th>'

    toptable += "</table>"
    return f"{FHEAD}{toptable}<h2>{pagename}</h2>{create_html_table(data, headingrow, columnalign, link, highlight, path=path, linkables=linkables, numberlink=numberlink, rotateheads=rotateheads, bigbold=bigbold)}"
