#!/usr/bin/env python3
import shutil
import math
import os

import html
from constants import *

from times import Time
from runner import Runner

PATHTOHTML = "SeniorWinterHandicap2019/"

MAINHEADS = ["Position", "Name", "Races Run", "Points"]
FASTHEADS = [
    "Position",
    "Name",
    "Time",
    "Race",
]
LEAGUEALIGNS = ["text-align:center", "text-align:left"]

RACEALIGNS = [
    "text-align:center",
    "text-align:left",
]


with open("dates.txt", "r") as f:
    DATES = [None] + f.read().split("\n")


def CSVtoTable(csv, sep=",", rowsep="\n"):
    """Convert a CSV string to a list of lists"""
    table = [row.split(sep) for row in csv.split(rowsep)]
    while [""] in table:
        table.remove([""])
    return table


def CSVtoDictTable(csv, headings):
    """Convert a CSV string to a list of dicts"""
    table = CSVtoTable(csv)
    return [{headings[i]: row[i] for i in range(len(headings))} for row in table]


def DictTabletoTable(table, headings):
    return [headings] + [[row[heading] for heading in headings] for row in table]


with open("age_grades.txt") as f:
    age_grades = CSVtoTable(f.read())


def AdjustTime(time, sex, age):
    """Adjust the time according to WMA table"""
    if age < 5:
        raise ValueError(f"age ({age}) < 5; invalid")
    elif age > 100:
        raise ValueError(f"age ({age}) > 100; invalid")

    if sex.lower() in ("m", "male", "boy"):
        factor = float(age_grades[age][0])
    elif sex.lower() in ("f", "female", "girl"):
        factor = float(age_grades[age][1])
    else:
        raise ValueError(f"Invalid sex: {sex}")

    return time * factor


if not os.path.exists(PATHTOHTML):
    os.makedirs(PATHTOHTML)

# Load the runner data
with open("runners.csv", "r") as f:
    runnerdata = CSVtoTable(f.read())

runners = {}

for row in runnerdata:
    #    # Remove first row as it's useless
    #    row.pop(0)  # Not currently needed
    # Convert the rows to Runner objects
    runners[row[0]] = Runner(*row)

racefiles = []

for race in range(1, RACECOUNT + 1):
    try:
        with open(f"results/{race}.csv", "r") as f:
            racefiles.append(
                (
                    CSVtoDictTable(
                        f.read(), ("Rank", "Name", "Actual Time", "Start Time")
                    ),
                    race,
                )
            )
    except FileNotFoundError:
        continue

validraces = [x[1] for x in racefiles]

for racefile in racefiles:
    race = racefile[1]
    r = f"Race {race}"
    racedata = racefile[0]
    position = 0
    for row in racedata:
        # Find the runner
        row["Number"] = race
        runner = runners[row["Name"]]
        if row["Rank"] == "":
            runner.start_time = Time(row["Start Time"])
            continue
        # Make times proper times
        row["Actual Time"] = Time(row["Actual Time"])
        # Add age category column
        row["Gender"] = runner.gender
        # Add the race to the corresponding runner
        runner.AddRace(row)
        # Use this to get average and best times
        row["Series Best"] = runner.GetBest()
        row["Average"] = runner.GetAverage()
        row["Estimated Time"] = WORSTHANDICAP - runner.start_time
        row["+/−"] = Time(row["Actual Time"] - row["Estimated Time"], "+")
        row["Adjusted Time"] = AdjustTime(
            row["Actual Time"], runner.gender, runner.AgeAt(DATES[race])
        )

    racedata = list(filter(lambda x: x["Rank"] != "", racedata))
    # Add the rows relevant to adjusted times
    newracedata = sorted(racedata, key=lambda x: x["Adjusted Time"])
    position = 0
    for row in newracedata:
        position += 1
        points = max(26 - position, 5)
        row["Points"] = points
        runners[row["Name"]].races[-1]["Points"] = points

    for sortmethod in SORTS:
        catdata = sorted(
            filter(SORTS[sortmethod][2], racedata),
            key=lambda x: x[SORTS[sortmethod][0]],
        )
        for pos, row in enumerate(catdata):
            row["Position"] = pos + 1
        racename = f"{r} – {sortmethod}"
        table = DictTabletoTable(catdata, SORTS[sortmethod][1])
        racefile = html.create_html_page(
            racename, table, sortmethod, r, validraces, columnalign=RACEALIGNS
        )
        with open(f"{PATHTOHTML}{racename}.html", "w") as f:
            f.write(racefile)

    # Make the next-race handicaps
    start_times = []
    for runner in runners.values():
        if runner.races:
            lasttime = Time(runner.races[-1]["Actual Time"], "00")
            predicted = Time(
                runner.RoundPB(
                    min(Time(WORSTHANDICAP - runner.start_time, "00"), lasttime)
                ),
                "00",
            )
        else:
            lasttime = ""
            predicted = Time(WORSTHANDICAP - runner.start_time, "00")
        row = []
        row.append(runner.name)
        row.append(" ".join(runner.name.split(" ")[1:]))
        row.append(Time(WORSTHANDICAP - runner.start_time, "00"))
        row.append(lasttime)
        row.append(predicted)
        t = runner.GetAverage(3)
        if t is not None:
            row.append(Time(t, "00"))
        else:
            row.append(None)
        t = runner.GetBest()
        if t is not None:
            row.append(Time(t, "00"))
        else:
            row.append(None)

        start_times.append(row)

    start_times.sort(key=lambda x: x[0])
    start_times.sort(key=lambda x: x[1])

    timespage = html.create_html_page(
        f"{r} – Start Times",
        [
            [
                "Name",
                "Surname",
                "Start Time for This Race",
                "Time",
                "Next Predicted Time",
                "Average Time",
                "Best Time",
            ]
        ]
        + start_times,
        "Start Times",
        "Start Times",
        [],
    )
    with open(f"{PATHTOHTML}StartTimes.html", "w") as f:
        f.write(timespage)

# Create league tables
table = [
    ["", runner.name, len(runner.races), runner.GetPoints(), runner.gender]
    for runner in runners.values()
]

# Filter out those with no races
table = list(filter(lambda x: x[2] > 0, table))

racename = "League Table"
# Not actual filtered
filtered = table.copy()
# Sort by PB, races, then points
filtered.sort(key=lambda x: x[2], reverse=True)
filtered.sort(key=lambda x: x[3], reverse=True)
for runner in range(len(filtered)):
    # Add the position
    if filtered[runner][3] == filtered[runner - 1][3]:
        filtered[runner][0] = "="
    else:
        filtered[runner][0] = str(runner + 1)
    # Remove the gender and category fields, as they shouldn't
    # be in the final table: they were just needed for filtering.
    filtered[runner] = filtered[runner][:-1]

racefile = html.create_html_page(
    "WMA League Table",
    [MAINHEADS] + filtered,
    "",
    racename,
    validraces,
    columnalign=LEAGUEALIGNS,
)
with open(f"{PATHTOHTML}{racename}.html", "w") as f:
    f.write(racefile)

# Make the best times pages
table = list(filter(lambda x: x.races, runners.values()))
table.sort(key=lambda x: x.GetBest())

for gender in GENDERS.values():
    racename = f"Fastest {gender}"
    filtered = list(filter(lambda x: x.gender == gender[0], table))
    pos = 1
    newtable = []
    for runner in filtered:
        newtable.append([pos, runner.name, runner.GetBest(), runner.GetBestIndex()])
        pos += 1

    racefile = html.create_html_page(
        f"Fastest Times {gender}",
        [FASTHEADS] + newtable,
        "",
        racename,
        validraces,
        columnalign=LEAGUEALIGNS,
    )
    with open(f"{PATHTOHTML}{racename}.html", "w") as f:
        f.write(racefile)
