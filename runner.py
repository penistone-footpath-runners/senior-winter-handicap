#!/usr/bin/env python3
from times import Time
import math
from constants import *


class Runner:
    def __init__(self, name, gender, date_of_birth, start_time):
        self.name = name
        self.gender = gender
        self.DOB = "-".join(date_of_birth.split("/")[::-1])
        self.PB = WORSTHANDICAP - Time(start_time)
        self.RoundPB()

        self.races = []
        self.StartTime(False)

    def AddRace(self, race):
        """Add a new race to the runner"""
        # Make the time the actual time
        start_time = self.StartTime(race["Start Time"])
        race["Finish Time"] = race["Actual Time"]
        race["Actual Time"] = race["Actual Time"] - start_time

        # Add the PB column
        race["Previous PB"] = str(self.PB)

        # Determine if the runner has a PB
        race["PB?"] = ""
        if self.PB is None or race["Actual Time"] < self.PB:
            self.PB = race["Actual Time"]
            self.RoundPB()

        self.races.append(race)

    def GetAverage(self, count=None):
        """Find the runner's average time this year"""
        if self.races:
            if count is None:
                counters = self.races
            else:
                counters = self.races[-count:]

            return round(
                Time(
                    sum([race["Actual Time"].time for race in counters]) / len(counters)
                )
            )
        else:
            return None

    def GetBest(self):
        """Find the best time the runner has achieved this year"""
        if self.races:
            return Time(min([race["Actual Time"].time for race in self.races]))
        else:
            return None

    def GetBestIndex(self):
        """Find the best time the runner has achieved this year"""
        best = self.GetBest()
        for race in self.races:
            if race["Actual Time"] == best:
                return race["Number"]

    def GetPoints(self):
        """Determine the total points of a runner"""
        if self.races:
            return sum([race["Points"] for race in self.races])
        else:
            return 0

    def RoundPB(self, val=None):
        """Round the PB to the nearest five seconds"""
        if val is None:
            self.effective_PB = Time(math.ceil(self.PB.time / 10) * 10)
        else:
            return Time(math.ceil(float(val / 10)) * 10)

    def StartTime(self, start_time):
        """Calculate the race's start time"""
        if not start_time:
            start_time = WORSTHANDICAP - self.effective_PB
        self.start_time = start_time
        return start_time

    def AgeAt(self, date):
        """Return the runner's age on a particular date"""
        approx = int(date.split("-")[0]) - int(self.DOB.split("-")[0])
        # Determine if their birthday has been reached this year
        sorty = [date[5:], self.DOB[5:]]
        if sorted(sorty) == sorty:
            return approx - 1
        return approx
