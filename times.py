#!/usr/bin/env python3
import math


class Time:
    def __init__(self, time, presentformat=None):
        self.presentformat = presentformat
        if isinstance(time, str):
            time = self.get_from_string(time)
        elif isinstance(time, Time):
            time = time.time

        # Ensure that there are (mostly) no floats
        if int(time) == time:
            time = int(time)
        self.time = time
        self.calculate_string()

    @staticmethod
    def get_from_string(time):
        """Find a time from a string"""
        if time == "":
            return 0
        if time[0] in ("-", "−"):
            negative = True
            time = time[1:]
        else:
            negative = False
        value = sum(
            (0 if x == "00" else int(x.lstrip("0"))) * 60 ** i
            for i, x in enumerate(time.strip().split(":")[::-1])
        )

        if negative:
            value = -value

        return value

    def calculate_string(self):
        """Make a string out of the time"""
        if self.time < 0:
            negative = True
            time = self.time * -1
        else:
            negative = False
            time = self.time
        time = round(time)
        if time < 6000:
            self.string = f"{time // 60}:{str(time % 60).zfill(2)}"
        else:
            self.hours = time // 3600
            self.secs = time % 3600
            self.string = f"{self.hours}:{str(self.secs // 60).zfill(2)}:{str(self.secs % 60).zfill(2)}"

        if negative:
            self.string = "−" + self.string

        elif self.presentformat == "+":
            self.string = "+" + self.string

        elif self.presentformat == "00":
            while len(self.string) < 8:
                if (len(self.string) + 1) % 3:
                    self.string = "0" + self.string
                else:
                    self.string = ":" + self.string

    def __add__(self, time):
        return Time(self.time + Time(time).time)

    def __sub__(self, time):
        return Time(self.time - Time(time).time)

    def __mul__(self, amount):
        return Time(self.time * amount)

    def __truediv__(self, amount):
        return Time(self.time / amount)

    def __lt__(self, time):
        return self.time < Time(time).time

    def __le__(self, time):
        return self.time <= Time(time).time

    def __eq__(self, time):
        try:
            othertime = Time(time).time
        except ValueError:
            return False
        return self.time == othertime

    def __ne__(self, time):
        return self.time != Time(time).time

    def __ge__(self, time):
        return self.time >= Time(time).time

    def __gt__(self, time):
        return self.time > Time(time).time

    def __repr__(self):
        return f"Time('{self.string}')"

    def __str__(self):
        return self.string

    def __int__(self):
        return round(self.time)

    def __float__(self):
        return float(self.time)

    def __round__(self):
        return Time(math.ceil(self.time))
